﻿<!-- Inicio Sub item 1 -->
<div class="container" id="situacao">
<br /><br /><br />
  <div class="row">
    <div class="col-md-6 col-sm-1">
      <img src="img/fita_autismo.jpg" class="img-fluid"/>
      <br /><br />

   </div>
   <div class="col-md-6">
     <h3>AMA</h3>
     <p style="text-align:justify;text-indent:30px">
	  AMA/REC-Associação dos Pais e Amigos dos Autistas de Criciúma, considerada uma instituição filantrópica que auxilia crianças, adolescentes e adultos diagnosticados com TEA (Transtorno do Espectro Autista). Atende 107 alunos, com idade mínima de 2 anos e máxima de 40 anos frequentando, 20% diagnosticado com autismo leve, 30% moderado e 50% severo, abrange 9 municípios da regional.
     </p>
  </div>
</div>
<!-- Final  Sub item 1 -->
<!-- Inicio Sub Menu 2 -->
<div class="container" id="mudancas">
<div class="row">
  <div class="col-md-4 col-sm-1">
    <img src="img/quebra_cabeca.jpg" / class="img-fluid"><br />
    <h4>Contribuição</h4>
    <p>
      A AMA pode contribuir com você e seu filho com TEA, facilitando seu acesso aos jogos educativos, envolvendo alfabetização, 
	   coordenação motora, incentivo da fala, raciocínio lógico, desenvolvimento neurológico, além de brincar com as letras 
	   e números.
    </p>
  </div>
  <div class="col-md-4">
    <img src="img/infinito.png" / class="img-fluid"><br />
    <h4>Missão do site</h4>
    <p>
    Compartilhar práticas utilizadas pela instituição, conscientizando a inclusão do público-alvo TEA no ensino regular. Divulgações 
	de eventos beneficentes, palestras e cursos ministrados pela mesma.
    </p>
  </div>
   <div class="col-md-4">
    <img src="img/coracao.jpg" / class="img-fluid"><br />
    <h4>TEA (Transtorno do Espectro Autista)</h4>
    <p>
    Considerado um transtorno neurológico que ocorre precocemente caracterizado por alterações significativas no desenvolvimento da comunicação, 
	interação social, cognitivo e no comportamento. Essas alterações levam a importantes dificuldades adaptativas que aparecem antes dos 3 anos 
	de idade, podendo ser percebidas, em alguns casos, já nos primeiros meses de vida, sendo mais comum o diagnóstico em crianças do sexo 
	masculino.
    </p>
  </div>
</div>
</div>
<!-- Final  Sub Menu 2 -->

<br /><br /><br /><br /><br /><br />