<?php
@session_start();
include("controladores/autenticacao.php");

$_SESSION['logado'] = autentica();

if($_GET['funcao'] == "sair"){
  $_SESSION['logado'] = false;
}

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>AMA </title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">

</head>

<body>
  <!-- Inicio - Menu Barra de Navegação -->

  <?php
    include('views/navbar.php');
  ?>

  <?php
  if (!isset($_GET['pagina'])) {
    include('views/home.php');
  } else {
    $pagina = $_GET['pagina'];
    include('views/' . $pagina . '.php');
  }
  ?>

</body>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>

</html>