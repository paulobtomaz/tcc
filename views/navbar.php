<nav class="navbar navbar-dark navbar-expand-md fundo ">
    <div class="container">
        <a href="?pagina=home" class="navbar-brand">
            <img src="img/car_trabalho1.png" /> AMA
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSite">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSite">
            <ul class="navbar-nav espaco_menu">
                <li class="nav-item">
                    <a href="?pagina=home" class="nav-link">Sobre</a>
                </li>
                <?php 
                    if($_SESSION['logado']){
                        echo '<li class="nav-item"><a href="?pagina=principal" class="nav-link">Principal</a></li>';
                        echo '<li class="nav-item"><a href="?pagina=atividades" class="nav-link">Atividades</a></li>';
                        echo '<li class="nav-item"><a href="?pagina=login&funcao=sair" class="nav-link">Sair</a></li>';
                    } else {
                        echo '<li class="nav-item"><a href="?pagina=login" class="nav-link">Login</a></li>';
                    }
                ?>
                <?php 
                    if($_SESSION['logado'][0]['tipo'] == 3){
                        include('views/menus/menu_logado.php');
                    } 
                ?>
            </ul>
        </div>
    </div>
</nav>