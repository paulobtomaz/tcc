<?php

$conexao = conectar();

if($_GET['insert']){

    $id = $_POST['id'];

    $nome = $_POST['nome'];
    $login = $_POST['login'];
    $senha = $_POST['senha'];
    $tipo = $_POST['tipo'];

    if($id){
        $sql = "UPDATE `usuarios` SET `nome`, `login`, `senha`, `tipo` WHERE id_usuario=$id";
        $result = mysqli_query($conexao, $sql);
    } else {
        $sql = "INSERT INTO `usuarios` (`nome`, `login`, `senha`, `tipo`) VALUES ('$nome', '$login', '$senha', $tipo)";
        $result = mysqli_query($conexao, $sql);
    }
}

if($_GET['delete']){
    $id = $_GET['delete'];
    //$sql = "DELETE FROM `usuarios` WHERE id_usuario=$id";
    $sql = "UPDATE `usuarios` SET tipo=0 WHERE id_usuario=$id";
    $result = mysqli_query($conexao, $sql);
}

if($_GET['select']){
    $id = $_GET['select'];
    $sql = "SELECT * FROM usuarios WHERE id_usuario=$id";
    $result = mysqli_query($conexao, $sql);
    $edit = mysqli_fetch_all($result, MYSQLI_ASSOC)[0];
}

$sql = "SELECT * FROM usuarios WHERE tipo = 2";
$result = mysqli_query($conexao, $sql);
$alunos = mysqli_fetch_all($result, MYSQLI_ASSOC);

?>

<div class="container">
    <form method="POST" action="index.php?pagina=admin/alunos&insert=true">
        <div>Nome: <input type=text name="nome" value="<?php echo $edit['nome']; ?>" /></div>
        <div>Login: <input type=text name="login" value="<?php echo $edit['login']; ?>" /></div>
        <div>Senha: <input type=text name="senha" value="<?php echo $edit['senha']; ?>" /></div>
        <div>Tipo: <input type=text name="tipo" value="<?php echo $edit['tipo']; ?>" /></div>
        <div><input type=submit name="update" value="Confirmar" /></div>
        <input type="hidden" name="id" value="<?php echo $_GET['select']; ?>">
    </form>

    <div class="listaAlunos">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th>Nome</th>
                    <th>Login</th>
                    <th>Tipo</th>
                    <th></th>
                </tr>
            </thead>
            <?php
            foreach ($alunos as $aluno) {
                echo "<tr>";
                echo "<td>" . $aluno['nome'] . "</td>";
                echo "<td>" . $aluno['login'] . "</td>";
                echo "<td>" . $aluno['tipo'] . "</td>";
                echo "<td><a href=?pagina=admin/alunos&select=" . $aluno['id_usuario'] . ">Editar</a> <a href=?pagina=admin/alunos&delete=" . $aluno['id_usuario'] . ">Deletar</a></td>";
                echo "</tr>";
            }
            ?>
        </table>
    </div>
</div>