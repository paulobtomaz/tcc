<?php
if(!$_SESSION['logado']){
	echo "Você precisa estar logado para acessar esta área do site";
	exit;
}
?>

<div class="container" class="principal">
	<div class="row">
        <?php

        $conexao = conectar();
        
        $sql = "SELECT * FROM atividades";
        $result = mysqli_query($conexao, $sql);
        $atividades = mysqli_fetch_all($result, MYSQLI_ASSOC);

        foreach ($atividades as $atividade) {   
        ?>
			
        <!-- Atividade -->
        <div class="col-md-4 atividade">
        <form action=<?php echo $atividade['link_acesso']; ?>>
        <img src="img/<?php echo $atividade['img']; ?>" / class="img-fluid"><br />
            <h4><?php echo $atividade['nome']; ?></h4>
            <p>
            <?php echo $atividade['descricao']; ?>
            </p>	
        
            <button class="btn btn-primary">Entrar na Atividade</button>
        </form>
        </div>
        <!-- Fim Atividade -->

        <?php } ?>

    </div>
</div>