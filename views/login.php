<div class="login-form">
    <form action="?pagina=principal" method="post">
      <h2 class="text-center">Login</h2>
      <div class="form-group">
        <input type="text" class="form-control" name="usuario" placeholder="Usuario" required="required">
      </div>
      <div class="form-group">
        <input type="password" class="form-control" name="senha" placeholder="Senha" required="required">
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-block">ENVIAR</button>
      </div>
    </form>
  </div>